import requests
from bs4 import BeautifulSoup
from pprint import pprint
from statistics import mean, median, mode
import time


allScores = []


def getPage(pagenum):
    time.sleep(5)
    thesescores = []
    data = requests.get(f'https://opencritic.com/browse/all?page={pagenum}')
    soup = BeautifulSoup(data.text, 'html.parser')
    all_scores = soup.findAll("div", {"class": "score"})
    for score in all_scores:
        if "?" in score:
            break

        thesescores.append(int(score.getText()))

    return thesescores


for i in range(1, 260):
    print(i)
    allScores.extend(getPage(i))


print(allScores)
print("mean", mean(allScores))
print("median", median(allScores))
print("mode", mode(allScores))